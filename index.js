const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

// Connect to database
mongoose.connect("mongodb+srv://admin:admin123@course-booking.m222n.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
});


// Check connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


// Create Schema
const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}

})

//Create Model
const User = mongoose.model("User", userSchema);


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create Route
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && result.username == req.body.username){			
			return res.send(`${req.body.username} already exists.`);
		} else {			
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			
			newUser.save((saveErr, savedTask) => {				
				if(saveErr){
					return console.error(saveErr)				
				} else {					
					return res.status(201).send("New User registered")
				}
			})
		}
	})
})


app.post('/login', (req, res) => {
	const userFound = User.findOne({username: req.body.username}, {password: req.body.password}, (err, result) => {
		if(result.username === req.body.username && result.password === req.body.password) {
			return res.send(`Welcome ${req.body.username}.`)
		} else {
			return res.status(500).send(`Invalid username and password.`)
		}
	})
	console.log(userFound)
})


app.listen(port, () => console.log(`Server running at port ${port}`));
